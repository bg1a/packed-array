Packed Array
============

Overview
--------
An implementation of a packed array of size NxN.

Details
-------
The maximum value of an array element is N. Each element shall
occupy a bitset in memory of predefined size. That size is
equal to the number of bits needed to represent N.
For instance, let's say N = 8, thus we need only 3 bits of memory
space for an element.
