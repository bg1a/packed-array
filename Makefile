TARGET = main
SRC_DIR = src
INC_DIR = inc

SRC = $(wildcard $(SRC_DIR)/*.cpp)
HDR = $(wildcard $(INC_DIR)/*.hpp)
OBJ = $(SRC:.cpp=.o)

CPPFLAGS := -I$(INC_DIR)
CPPFLAGS += -DDEBUG_PACKED_ARRAY
CPPFLAGS += -g3 -gdwarf-2
CPPFLAGS += -Werror -Wall
CPPFLAGS += -O -O3

PHONY := all
all: $(OBJ)
	$(CXX) -o $(TARGET) $(OBJ)

*.o: *.cpp

PHONY += clean
clean:
	@rm -f $(OBJ) $(TARGET)

.PHONY: $(PHONY)
