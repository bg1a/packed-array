#include "PackedArr.hpp"
using namespace packed_array;

int main(int argc, char** argv)
{
    PackedArr pa(33);
    pa[0][5] = 15;
    const PackedArr pc = pa;

    PE(pc[0][5]);
    PE(pa[0][5]);
    PE(pa[0][5] = 2);
    PE(pa[0][5] += 3);
    PE(pa[0][5] = pc[0][5]);
    PE(pa[0][5] -= 10);
    PE(pa[0][5] *= 2);
    PE(pa[0][5] = 2);
    PE(pa[0][5] = pc[0][5] * 2);
    PE(pa[0][5] /= 2);
    PE(pa[0][5] %= 2);

    PE(pa[0][5] ^= -1UL);
    PE(pa[0][5] &= -1UL);
    PE(pa[0][6]++);
    PE(++pa[0][4]);
    PE(pa[0][6]);

    PE(pa[0][6]--);
    PE(pa[0][6]);
    PE(--pa[0][5]);

    PS("Fill array with values:\n");
    int i, j;
    for(i = 0; i < 33; i++)
        for(int j = 0; j < 33; j++)
            pa[i][j] = j;

    PS("Array is:\n");
    for(i = 0; i < 33; i++)
    {
        PS("\n");
        for(j = 0; j < 33; j++)
            if(j < 10)
                std::cout << pa[i][j] << "  ";
            else
                std::cout << pa[i][j] << " ";
    }
    PS("\n");
    PE(pa[0][15] = 4);
    PE(pa[0][15]);
    PE(pa[42][10]);

    PE(pa.getCap());
    PE(pa.getULsize());
    PE(pa.getBits());
    return 0;

}
