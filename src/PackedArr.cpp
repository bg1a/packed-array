#include "PackedArr.hpp"

using namespace packed_array;

PackedArr::ArrayBuf::ArrayBuf(int elements)
: N(elements), refCount(1)
{
    bits = log2ceil(N);
    size = (bits * N * N) / lsize + 1;
    data = new unsigned long[size];
}

PackedArr::ArrayBuf::ArrayBuf(const ArrayBuf& rhs)
: N(rhs.N), bits(rhs.bits), size(rhs.size), refCount(1) 
{
    data = new unsigned long[size];
    memcpy(data, rhs.data, size);
}

PackedArr::ArrayBuf::~ArrayBuf()
{
    delete [] data;
}

PackedArr::PackedArr(int n)
: buf(new PackedArr::ArrayBuf(n))
{}

PackedArr::PackedArr(const PackedArr& rhs)
: buf(rhs.buf) 
{
    ++buf->refCount;
}

PackedArr::~PackedArr()
{
    if(--buf->refCount == 0)
        delete buf;
}

PackedArr& PackedArr::operator=(const PackedArr& rhs)
{
    if(buf == rhs.buf)
        return *this;
    
    if(--buf->refCount == 0)
        delete buf;
    buf = rhs.buf;
    ++buf->refCount;
    return *this;
}

PackedArr::ArrayProxy::ArrayProxy(PackedArr& pa, int raw_idx)
: arr(pa), i(raw_idx) 
{}

PackedArr::ArrayProxy PackedArr::operator[](int i)
{
    return ArrayProxy(*this, i);   
}

const PackedArr::ArrayProxy PackedArr::operator[](int i) const
{
    return ArrayProxy(const_cast<PackedArr&>(*this), i);
}

const unsigned long PackedArr::ArrayProxy::operator[](int j) const
{
    return arr.get(i, j);
}

PackedArr::ArrayProxy& PackedArr::ArrayProxy::operator[](int j)
{
    this->j = j;
    return *this;
}

PackedArr::ArrayProxy& PackedArr::ArrayProxy::operator=(unsigned long new_val)
{
    arr.set(i, j, new_val);
    return *this;
}

PackedArr::ArrayProxy& PackedArr::ArrayProxy::operator+=(unsigned long new_val)
{
    arr.set(i, j, arr.get(i, j) + new_val);
    return *this;
}

PackedArr::ArrayProxy& PackedArr::ArrayProxy::operator-=(unsigned long new_val)
{
    arr.set(i, j, arr.get(i, j) - new_val);
    return *this;
}

PackedArr::ArrayProxy& PackedArr::ArrayProxy::operator*=(unsigned long new_val)
{
    arr.set(i, j, arr.get(i, j) * new_val);
    return *this;
}

PackedArr::ArrayProxy& PackedArr::ArrayProxy::operator/=(unsigned long new_val)
{
    arr.set(i, j, arr.get(i, j) / new_val);
    return *this;
}

PackedArr::ArrayProxy& PackedArr::ArrayProxy::operator%=(unsigned long new_val)
{
    arr.set(i, j, arr.get(i, j) % new_val);
    return *this;
}

PackedArr::ArrayProxy& PackedArr::ArrayProxy::operator^=(unsigned long new_val)
{
    arr.set(i, j, arr.get(i, j) ^ new_val);
    return *this;
}

PackedArr::ArrayProxy& PackedArr::ArrayProxy::operator|=(unsigned long new_val)
{
    arr.set(i, j, arr.get(i, j) | new_val);
    return *this;
}

PackedArr::ArrayProxy& PackedArr::ArrayProxy::operator&=(unsigned long new_val)
{
    arr.set(i, j, arr.get(i, j) & new_val);
    return *this;
}

PackedArr::ArrayProxy& PackedArr::ArrayProxy::operator>>=(unsigned long new_val)
{
    arr.set(i, j, arr.get(i, j) >> new_val);
    return *this;
}

PackedArr::ArrayProxy& PackedArr::ArrayProxy::operator<<=(unsigned long new_val)
{
    arr.set(i, j, arr.get(i, j) << new_val);
    return *this;
}

PackedArr::ArrayProxy& PackedArr::ArrayProxy::operator++()
{
    arr.set(i, j, arr.get(i, j) + 1UL);
    return *this;
}

unsigned long PackedArr::ArrayProxy::operator++(int)
{
    unsigned long tmp = arr.get(i, j);
    arr.set(i, j, arr.get(i, j) + 1UL);
    return tmp;
}

PackedArr::ArrayProxy& PackedArr::ArrayProxy::operator--()
{
    arr.set(i, j, arr.get(i, j) - 1UL);
    return *this;
}

unsigned long PackedArr::ArrayProxy::operator--(int)
{
    unsigned long tmp = arr.get(i, j);
    arr.set(i, j, arr.get(i, j) - 1UL);
    return tmp;
}
PackedArr::ArrayProxy::operator unsigned long int()
{
    return arr.get(i, j);
}

int PackedArr::ArrayBuf::validIdx(int i, int j)
{
    int res = 0;
    if((i < N) && (i >= 0) &&
            (j < N) && (j >= 0))
        res = 1;
    
    return res;
}

unsigned long PackedArr::get(int i, int j)
{
    if(buf->validIdx(i, j))
        return buf->get(i, j);
    else
        return -1UL;
}

unsigned long PackedArr::ArrayBuf::get(int i, int j)
{
    int bit_off = (i * N + j) * bits;
    int loff = bit_off / lsize;
    long d = bit_off % lsize + bits;
    unsigned long msk = -1UL >> bit_off % lsize;
    unsigned long l1 = data[loff];
    unsigned long l2 = data[loff + 1];

    if(d > lsize)
    {
        l1 &= msk;
        l1 <<= d - lsize;
        l2 >>= 2*lsize - d;
        l1 |= l2;
    }else{
        l1 &= msk;
        l1 >>= lsize - d;
    }
    return l1;
}

void PackedArr::set(int i, int j, int n)
{
    if(buf->validIdx(i, j))
    {
        if(buf->refCount > 1)
        {
            --buf->refCount;
            buf = new ArrayBuf(*buf);
        }
        buf->set(i, j, n);
    }
}

void PackedArr::ArrayBuf::set(int i, int j, int n)
{
    unsigned long n1, n2;
    int bit_off = (i * N + j) * bits;
    int loff = bit_off / lsize;
    long d = bit_off % lsize + bits;
    unsigned long msk = -1UL >> bit_off % lsize;
    unsigned long l1 = data[loff];
    unsigned long l2 = data[loff + 1];

    if(d > lsize)
    {
        n1 = n >> (d - lsize);
        l1 &= ~msk;
        l1 |= n1 & msk;
        msk = -1UL << (2*lsize - d);
        n2 = n << (2*lsize - d);
        l2 &= ~msk;
        l2 |= n2;
        data[loff + 1] = l2;
    }else if(d == lsize){
        l1 &= ~msk;
        l1 |= n & msk;
    }else{
        msk &= ~(-1UL >> d);
        l1 &= ~msk;
        n <<= lsize - d;
        l1 |= n & msk;
    }
    data[loff] = l1;
}

// Get a raw/column size of an array
int PackedArr::getCap(void)
{
    return buf->N;
}

// Get a quantity of unsigned long elements used for data buffer
int PackedArr::getULsize(void)
{
    return buf->size;
}

// Get a qauntity of bits used for a single element in an array
int PackedArr::getBits(void)
{
    return buf->bits;
}

int PackedArr::nlz(unsigned int x)
{
    unsigned int y;
    int n = 32;
    y = x >> 16; if(y != 0 ) {n = n - 16; x = y;}
    y = x >> 8; if(y != 0 ) {n = n - 8; x = y;}
    y = x >> 4; if(y != 0 ) {n = n - 4; x = y;}
    y = x >> 2; if(y != 0 ) {n = n - 2; x = y;}
    y = x >> 1; if(y != 0 ) return n - 2;
    return n - x;
}
int PackedArr::log2bot(unsigned int x)
{
    return 31 - nlz(x);
}
int PackedArr::log2ceil(unsigned int x)
{
    return 32 - nlz(x - 1);
}
