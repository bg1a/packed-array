#ifndef __MAIN_HPP__
#define __MAIN_HPP__

#include <stdlib.h>
#include <iostream>
#include <iomanip>

#include <string.h>
#include <limits>
#ifdef DEBUG_PACKED_ARRAY
#define XSTR(_a) #_a
#define STR(_a) XSTR(_a)
#define PE(_a) do{std::cout << STR(_a) << " : " << (_a) << std::endl;}while(0)
#define PS(_a) do{std::cout << (_a);}while(0)
#else
#define PE(_a)
#define PS(_a)
#endif /* DEBUG_PACKED_ARRAY */

namespace packed_array
{
    const long lsize = std::numeric_limits<unsigned long>::digits;
    class PackedArr
    {
        struct ArrayBuf{
            int N;
            int bits;
            int size;
            unsigned long *data;
            int refCount;
            ArrayBuf(int elements);
            ArrayBuf(const ArrayBuf& rhs);
            ~ArrayBuf();
            int validIdx(int i, int j);
            unsigned long get(int i, int j);
            void set(int i, int j, int n);
        }*buf;

        PackedArr(){}

        public:
            explicit PackedArr(int n);
            PackedArr(const PackedArr& rhs);
            ~PackedArr();

            class ArrayProxy
            {
                private:
                    PackedArr& arr;
                    int i, j;
                
                    unsigned long * operator&();
                    const unsigned long * operator&() const;
                public:
                    ArrayProxy(PackedArr& pa, int raw_idx);

                    const unsigned long operator[](int j) const;
                    ArrayProxy& operator[](int j);
                    ArrayProxy& operator=(unsigned long new_val);
                    ArrayProxy& operator+=(unsigned long new_val);
                    ArrayProxy& operator-=(unsigned long new_val);
                    ArrayProxy& operator*=(unsigned long new_val);
                    ArrayProxy& operator/=(unsigned long new_val);
                    ArrayProxy& operator%=(unsigned long new_val);
                    ArrayProxy& operator^=(unsigned long new_val);
                    ArrayProxy& operator|=(unsigned long new_val);
                    ArrayProxy& operator&=(unsigned long new_val);
                    ArrayProxy& operator<<=(unsigned long new_val);
                    ArrayProxy& operator>>=(unsigned long new_val);
                    ArrayProxy& operator++();
                    unsigned long operator++(int);
                    ArrayProxy& operator--();
                    unsigned long operator--(int);
                    operator unsigned long int();
            };
            
            friend ArrayProxy;

            ArrayProxy operator[](int i);
            const ArrayProxy operator[](int i) const;
            PackedArr& operator=(const PackedArr& rhs);

            static int nlz(unsigned int x);
            static int log2ceil(unsigned int x);
            static int log2bot(unsigned int x);
            unsigned long get(int i, int j);
            void set(int i, int j, int n);
            int getCap(void);
            int getULsize(void);
            int getBits(void);
    };
}
#endif /* __MAIN_HPP__ */
